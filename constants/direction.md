# Direction constants

Direction constants describe the facing or movement direction of an entity such as a player or NPC.

| Constant | Value | Description |
| --- | --- | --- |
| DIR_LEFT | -1 | Left direction. |
| DIR_RANDOM | 0 | Random direction for NPCs. |
| DIR_RIGHT | 1 | Right direction. |